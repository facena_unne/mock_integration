/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sol.mail;

import com.sol.dep.AddressException;
import com.sol.dep.InternetAddress;
import com.sol.mail.pojo.DireccionDeCorreo;
import org.junit.After;
import org.junit.Test;

//!!! stactic import
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mockito;

//!!! stactic import
import static org.mockito.Mockito.times;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoRule;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

/**
 *
 * @author Pablo N. Garcia Solanellas
 */


//activamos mockito con esta linea avilitamos las anotaciones de mockito
@RunWith(MockitoJUnitRunner.class)
public class DireccionDeCorreoTest {
    
    @InjectMocks
    DireccionDeCorreo dir = new DireccionDeCorreo();//dependiente Solo puede haber uno
 
    /* puedo invocar una dependencia que sirva al todo el test.
    @Mock
    InternetAddress iAddress; 
    */
    
    // Test Unitario del metodo esValido() de la clase DireccionDeCorreo
    // en este caso no hay dependencias externas.
    @Test
    public void DireccionCorreoTest() 
    {        
        //estos deberian pasar la prueba        
        assertTrue(new DireccionDeCorreo("pablo@gmail.com").esValido());
        assertTrue(new DireccionDeCorreo("pablo@gmail.com.ar").esValido());
        assertTrue(new DireccionDeCorreo("pablo-garcia@gmail.com.ar").esValido());
        assertTrue(new DireccionDeCorreo("pablo-garcia@yahoo-test.com.ar").esValido());
        
        
        //INVALIDAS                 
        assertFalse(new DireccionDeCorreo("pablo..@gmail.com.a1").esValido());
        assertFalse(new DireccionDeCorreo("pablogmail.com").esValido());
        assertFalse(new DireccionDeCorreo("pablo@.com").esValido());
        
    }
    
    /**
     * Test de INTEGRACION Con depependcencias externas
     * esperamos:
     * Un caso en en que el correo es valido
     * Un caso en el que el correo es InValido.
     */
    @Test()
    public void IAddressTest() {                
           
        //Con el Framework Mockito conseguimos una simulacion de los 
        //objetos que necesitamos.
        //Dependencia. 
        InternetAddress iAddress = Mockito.mock(InternetAddress.class);
        
        try {

            //definimos el comportamiento del la funcion isValid en caso de recibir un string Cualquiera..
            //esperamos que el primer llamado sea valido y el segundo in valido.
            Mockito.when(iAddress.isValid(Matchers.anyString())).thenReturn(true,false);
            
            //Instanciamos el DEPENDIENTE
            DireccionDeCorreo _correoValido = new DireccionDeCorreo(iAddress,"pablo@gmail.com");

            //Intereactuamos con la clases.
            //primer llamado.
            assertTrue(_correoValido.isValidEmailAddress());
            
            //segundo llamado.
            DireccionDeCorreo _correoNOvalido = new DireccionDeCorreo(iAddress,"pablo@.com");
            assertFalse(_correoNOvalido.isValidEmailAddress());
            
            /**aqui es donde la Magia sucede, verificamos si el metodo isValid
             * de la DEPENDENCIA fue invocado uno numero determinado de 
             * veses n  times.
             */            
            Mockito.verify(iAddress, times(2) ).isValid(Matchers.anyString());
            
            //Mockito.verify(iAddress, times(1) ).isValid("pablo@gmail.com");
            //Mockito.verify(iAddress, times(1) ).isValid("pablo@.com");
            
        } catch (AddressException ex) {
            /**
             * Como probamos datos que puende ser manejados por el metodo que 
             * estamos probando. Esperamos que no se lance una Excepcion
             */
            fail("IAddressTest:: Aqui no deberia dispararce ninguna Exception ");
        }  
    }
    
    /**
     * Test de INTEGRACION Con depependcencias externas
     * Probamos la ocurrencia de una excepcion.
     */
    @Test()
    public void IAddressExceptionTest() {                
           
        //Con el Framework Mockito conseguimos una simulacion de los 
        //objetos que necesitamos.
        //Dependencia. 
        InternetAddress iAddress = Mockito.mock(InternetAddress.class);
              
        try {
            
            //MOCK de la DEPENDENCIA.
            //modelamos el comportamiento del metodo isValid() en caso de recibir null como parametro
            Mockito.when(iAddress.isValid(null)).thenThrow(new AddressException()); // matches a whitespace character
            
            //Instanciamos el DEPENDIENTE
            DireccionDeCorreo _correoNULO = new DireccionDeCorreo(iAddress,null);
            
            //Intereactuamos con la clases.
            _correoNULO.isValidEmailAddress();
            
            // si la ejecuccion llega hasta este punto algo salio mal.
            //Antes se tendria que haber lanzado la excepcion.
            assertTrue(false);
        } catch (AddressException ex) {
            // Este es el resultado esperado.
            assertTrue(true);
        }
    }
    
    /**
     * Test de INTEGRACION Con depependcencias externas
     *
     */
    @Test()
    public void hardCodeAnswerTest() {                
           
        //Con el Framework Mockito conseguimos una simulacion de los 
        //objetos que necesitamos.
        //Dependencia. 
        InternetAddress iAddress = Mockito.mock(InternetAddress.class);
        
        try {

            //definimos el comportamiento del la funcion isValid en caso de recibir un string Cualquiera..
            //esperamos que el primer llamado sea valido y el segundo in valido.
            Mockito.when(iAddress.isValid(Matchers.anyString())).then(
                new Answer<Boolean>(){
                    @Override
                    public Boolean answer(InvocationOnMock invocation) throws AddressException{
                        if(true){
                            throw new AddressException();
                        }
                        return false;
                    }
                }
            );
            
            //Instanciamos el DEPENDIENTE
            DireccionDeCorreo _correoValido = new DireccionDeCorreo(iAddress,"pablo@gmail.com");

            //Intereactuamos con la clases.
            //primer llamado.
            _correoValido.isValidEmailAddress();
            
            /**aqui es donde la Magia sucede, verificamos si el metodo isValid
             * de la DEPENDENCIA fue invocado uno numero determinado de 
             * veses n  times.
             */            
            Mockito.verify(iAddress, times(2) ).isValid(Matchers.anyString());
            
        } catch (AddressException ex) {
            //
            assertTrue(true);
            //fail("OWNIAddressTest: Aqui no deberia dispararce ninguna Exception ");
        }  
    }
}
