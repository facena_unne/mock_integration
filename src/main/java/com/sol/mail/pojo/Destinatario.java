/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sol.mail.pojo;

/**
 *
 * @author Pablo N. Garcia Solanellas
 */
public class Destinatario {
        
    //nombre del destinatario, con propositos de agenda
    private String nombre;
    //direccion de correo electronico del destinatario (e-mail).
    private DireccionDeCorreo direccion;    
    
    
    public Destinatario(){}
    
    /**
     * Instancioamos un destinatario del cual no conosemos el nombre,
     * @param p_direccion 
     */
    public Destinatario(DireccionDeCorreo p_direccion)
    {    
        this.setDireccion(p_direccion);
    }
    /**
     * Intanciamos un destinatario del cual conocemos el nombre,      
     * @param p_nombre nombre del destinatario. 
     * @param p_direccion 
     */
    public Destinatario(String p_nombre, DireccionDeCorreo p_direccion)
    {
        this.setNombre(p_nombre);
        this.setDireccion(p_direccion);
    }
    
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public DireccionDeCorreo getDireccion() {
        return direccion;
    }

    public void setDireccion(DireccionDeCorreo direccion) {
        this.direccion = direccion;
    }
    
    
    
}
