/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sol.mail.pojo;

import java.util.List;

/**
 *
 * @author Pablo N. Garcia Solanellas
 */
public class Mensage {
    private String titulo;
    private Remitente remitente;
    private List<Destinatario> destinatarios;
    private String plantilla; //template
    private String tipo;

    public Mensage(){}
    
    public Mensage(String p_titulo, Remitente p_remitente,
        String p_plantilla)
    {
        this.setTitulo(p_titulo);
        this.setRemitente(p_remitente);
        this.setPlantilla(p_plantilla);        
        this.setTipo("text/html; charset=utf-8");
    }
    
    public Mensage(String p_titulo, Remitente p_remitente,
        String p_plantilla, String p_tipo)
    {
        this.setTitulo(p_titulo);
        this.setRemitente(p_remitente);
        this.setPlantilla(p_plantilla);        
        this.setTipo(p_tipo);
    }
    
    public boolean agregarDestinatario(Destinatario p_destinatario){
        return this.getDestinatarios().add(p_destinatario);
    }
    
    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Remitente getRemitente() {
        return remitente;
    }

    public void setRemitente(Remitente remitente) {
        this.remitente = remitente;
    }

    public List<Destinatario> getDestinatarios() {
        return destinatarios;
    }

    public void setDestinatarios(List<Destinatario> destinatarios) {
        this.destinatarios = destinatarios;
    }

    public String getPlantilla() {
        return plantilla;
    }

    public void setPlantilla(String plantilla) {
        this.plantilla = plantilla;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
    
    
    
}
