/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sol.mail.pojo;

import com.sol.dep.AddressException;
import com.sol.dep.InternetAddress;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Pablo N. Garcia Solanellas
 */
public class DireccionDeCorreo {
    
    // Esperamos la implementacion de la clase InternetAddress 
    private InternetAddress iAddress;// dependencia
    private String direccion;
    
    public DireccionDeCorreo(){}
    public DireccionDeCorreo(String p_direccion){
        this.setDireccion(p_direccion);
    }
    public DireccionDeCorreo( InternetAddress p_iAddress, String p_direccion){
        this.setiAddress(p_iAddress);
        this.setDireccion(p_direccion);
    }
    /**
     * Fuente:https://stackoverflow.com/questions/624581/what-is-the-best-java-email-address-validation-method
     * @return booelan true si la direccion tiene el formato correcto.
     * @deprecated 
     */
    public boolean esValido() 
    {        
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        Pattern p = Pattern.compile(ePattern);
        Matcher m = p.matcher(this.getDireccion());
        return m.matches();
    }
    
    /**
     * Verifica si el contenido del atributo direccion es una 
     * direccion de internet valida.
     * @return boolean 
     */
    public boolean isValidEmailAddress()
        throws AddressException
    {
        return this.getiAddress().isValid(this.getDireccion());
    }
    
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public InternetAddress getiAddress() {
        return iAddress;
    }

    public void setiAddress(InternetAddress iAddress) {
        this.iAddress = iAddress;
    }
    
    
}
