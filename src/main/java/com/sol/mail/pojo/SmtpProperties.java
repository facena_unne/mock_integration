/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sol.mail.pojo;

import java.util.Properties;

/**
 *
 * @author Pablo N. Garcia Solanellas
 */
public class SmtpProperties {
    private boolean autenticacion;
    private boolean tsl;
    private String host;
    private int puerto;
    
    /**
     * Constructor inicia la configuracion para el servidor de Gmail
     */
    public SmtpProperties(){
        this.setAutenticacion(true);
        this.setTsl(true);
        this.setHost("smtp.gmail.com");
        this.setPuerto(587);
    };
    public SmtpProperties(String p_host, int p_puerto)
    {
        this.setAutenticacion(true);
        this.setTsl(true);
        this.setHost(p_host);
        this.setPuerto(p_puerto);
    }
    public SmtpProperties(boolean p_autenticacion, boolean p_tsl,
        String p_host, int p_puerto)
    {
        this.setAutenticacion(p_autenticacion);
        this.setTsl(p_tsl);
        this.setHost(p_host);
        this.setPuerto(p_puerto);
    }
    
    
    public Properties properties(){
        Properties _props = new Properties();
        _props.put("mail.smtp.auth", this.isAutenticacion());
        _props.put("mail.smtp.starttls.enable", this.isTsl());
        _props.put("mail.smtp.host", this.getHost());
        _props.put("mail.smtp.port", this.getPuerto());
        return _props;
    }
    
    public boolean isAutenticacion() {
        return autenticacion;
    }

    public void setAutenticacion(boolean autenticacion) {
        this.autenticacion = autenticacion;
    }

    public boolean isTsl() {
        return tsl;
    }

    public void setTsl(boolean tsl) {
        this.tsl = tsl;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPuerto() {
        return puerto;
    }

    public void setPuerto(int puerto) {
        this.puerto = puerto;
    }
}
