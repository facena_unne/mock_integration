/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sol.commons;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

/**
 *
 * @author Pablo N. Garcia Solanellas
 */
public class Parser {    
    
    // formato por defecto para la fecha. [fecha mysql]
    private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";
    
    public static String dateToJSON(Date p_fehca){        
        return Parser.dateToJSON(p_fehca,DEFAULT_DATE_FORMAT);
    }
    public static String dateToJSON(Date p_fehca, String p_format){        
        if (p_fehca!= null){
            SimpleDateFormat sdf = new SimpleDateFormat(p_format!=null ? p_format : DEFAULT_DATE_FORMAT);        
            return sdf.format( p_fehca );
        }else{
            return null;
        }        
    }
    
    public static Integer toInteger(String p_data, String p_attrName, Integer p_default){
        try{
            return p_data.isEmpty()?p_default:Integer.parseInt(p_data);
        }catch(NumberFormatException nfg){
            throw new NumberFormatException("El campo "+p_attrName + " no tiene el formato esperado");
        }
    }    
    public static Integer toInteger(String p_data, String p_attrName){
        try{
            return p_data.isEmpty()?null:Integer.parseInt(p_data);
        }catch(NumberFormatException nfg){
            throw new NumberFormatException("El campo "+p_attrName + " no tiene el formato esperado");
        }
    }
    public static Long toLong(String p_data, String p_attrName, Long p_default){
        try{
            return p_data.isEmpty()?p_default:Long.parseLong(p_data);
        }catch(NumberFormatException nfg){
            throw new NumberFormatException("El campo "+p_attrName + " no tiene el formato esperado");
        }
    }
    public static Long toLong(String p_data, String p_attrName){
         try{
            return p_data.isEmpty()?null:Long.parseLong(p_data);
        }catch(NumberFormatException nfg){
            throw new NumberFormatException("El campo "+p_attrName + " no tiene el formato esperado");
        }
    }
    
    public static Double toDouble(String p_data, String p_attrName, Double p_default){
        try{
            return p_data.isEmpty()?p_default:Double.parseDouble(p_data);
        }catch(NumberFormatException nfg){
            throw new NumberFormatException("El campo "+p_attrName + " no tiene el formato esperado");
        }
    }
    public static Double toDouble(String p_data, String p_attrName){
        try{
            return p_data.isEmpty()?null:Double.parseDouble(p_data);
        }catch(NumberFormatException nfg){
            throw new NumberFormatException("El campo "+p_attrName + " no tiene el formato esperado");
        }
    }
    
}
