/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sol.dep;

/**
 * Modela el comla el contenido de la clase InternetAddress
 * @since javax.mail.internet.InternetAddress dependency
 * @author Pablo N. Garcia Solanellas
 */
public interface InternetAddress {
    
    void validate() throws AddressException;
    boolean isValid(String p_address) throws AddressException;
    
}
